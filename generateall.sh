#!/usr/bin/env bash

echo "Alles generieren"

gen() {
  rm -rf $1$3/*.svg
  rm -rf $1$3/*.png
  for f in $1/*
  do
    type=$(file -b $f)
    if [ "UTF-8 Unicode text" = "$type" ]; then
      echo "Verarbeite $f ..."
      ./generate.sh $f $2 $3
    fi
  done
}

gen klein template-klein.svg
#gen groß template-groß.svg
#gen gross template-klein.svg
#gen gross template-groß2.svg Unten

echo "Alles fertig."
