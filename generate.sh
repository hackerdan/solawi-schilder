#!/usr/bin/env bash

echo "Generieren"

usage() {
  echo "Verwendung: $0 <dateiname> <template> [<suffix>]"
  echo "wobei <dateiname> mindestens 2 Zeilen Text enthalten muss, die erste Zeile wird die Überschrift, die weiteren Zeilen (bis zu 7) der Text"
}

if [ $# -lt 2 ]; then
  usage
  echo "Dateiname und/oder SVG-Template fehlt."
  exit 1
fi

suffix=
if [ $# -eq 3 ]; then
  suffix=$3
fi

filename=$1
lineCount=$(wc -l $filename | awk '{print $1}')

if [ $lineCount -lt 1 ]; then
  usage
  echo "Zu wenig Zeilen: $lineCount"
  exit 1
fi

input=$2
svg=$filename$suffix.svg

readarray -t lines < $filename
arrayLen=${#lines[@]}

echo "Texte ersetzen ..."
sed "s#>HEADING<#>${lines[0]}<#g" $input > $svg
for i in {1..7}
do
  sed -i "s#>Z${i}<#>${lines[${i}]}<#g" $svg
done

echo "Hershey Text anwenden und Überschrift zentrieren ..."
grep HEADING $input
center=$?
inkscape \
--verb=EditDeselect \
--verb=EditSelectAll \
--verb=org.evilmad.text.hershey.noprefs \
--verb=FileSave --verb=FileClose --verb=FileQuit $svg 2>/dev/null
headlineId=$(xmlstarlet sel -N s=http://www.w3.org/2000/svg -t -v "/s:svg/s:g/s:g[last()]/@id" $svg)
# headline nur zentrieren, wenn eine im Template drin ist
if [ $center -eq 0 ]; then
  inkscape \
  --verb=EditDeselect \
  --select=$headlineId \
  --verb=AlignVerticalCenter \
  --verb=FileSave --verb=FileClose --verb=FileQuit $svg 2>/dev/null
fi
inkscape \
--verb=EditSelectAll \
--verb=SelectionUnGroup \
--verb=EditSelectAll \
--verb=SelectionUnGroup \
--verb=EditSelectAll \
--verb=SelectionUnGroup \
--verb=FileSave --verb=FileClose --verb=FileQuit $svg 2>/dev/null

echo "Vorschau-PNG erzeugen ..."
png=$filename$suffix.png
cp $svg tmp.svg
sed -i 's/stroke-width:0.00175.*in;stroke-linecap:round;stroke-linejoin:round/stroke-width:3.175;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none/g' tmp.svg
sed -i 's/stroke-width:0.00364.*in;stroke-linecap:round;stroke-linejoin:round/stroke-width:3.175;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none/g' tmp.svg
inkscape -z tmp.svg -e $png 2>/dev/null
mogrify -rotate "90" $png
rm tmp.svg

echo "Fertig."